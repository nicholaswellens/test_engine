# frozen_string_literal: true

module Avo
  module ExternalResourceConcern
    extend ActiveSupport::Concern

    def pagy_get_items(collection, _pagy)
      collection.all
    end

    def pagy_get_vars(collection, vars)
      collection.where(page: page, size: per_page)

      vars[:count] = collection.all.count
      vars[:page] = params[:page]
      vars
    end

    private

    def per_page
      params[:per_page] || Avo.configuration.per_page
    end

    def page
      params[:page]
    end
  end
end
