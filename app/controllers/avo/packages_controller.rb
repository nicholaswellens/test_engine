# frozen_string_literal: true

module Avo
  class PackagesController < Avo::ResourcesController
    include Avo::ExternalResourceConcern
    authorize_active_resource_requests
  end
end
