# frozen_string_literal: true

module Avo
  class PackageEventsController < Avo::ResourcesController
    include Avo::ExternalResourceConcern
    authorize_active_resource_requests
  end
end
