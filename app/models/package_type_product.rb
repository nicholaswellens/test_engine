# frozen_string_literal: true

# == Schema Information
#
# Table name: package_types_products
#
#  id              :bigint           not null, primary key
#  people          :integer          not null
#  quantity        :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  package_type_id :bigint           not null
#  product_id      :bigint           not null, indexed
#
class PackageTypeProduct < ApplicationRecord
  self.table_name = "package_types_products"

  belongs_to :product

  class << self
    def ransackable_attributes(auth_object = nil)
      ["product_id", "package_type_id"]
    end

    def ransackable_associations(auth_object = nil)
      []
    end
  end
end
