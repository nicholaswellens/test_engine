# frozen_string_literal: true

module Core
  class Base < ActiveResource::Base
    self.site = "#{ENV["ZEROO_CORE_URL"]}/api/v1"

    # removes .json from end of API request URL
    self.include_format_in_path = false

    class << self
      def column_names
        self::COLUMN_NAMES
      end

      def base_class
        name.demodulize
      end
    end
  end
end
