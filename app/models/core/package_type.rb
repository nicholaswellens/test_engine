# frozen_string_literal: true

module Core
  class PackageType < Base
    COLUMN_NAMES = ["id", "name", "size", "volume"]
  end
end
