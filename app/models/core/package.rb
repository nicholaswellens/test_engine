# frozen_string_literal: true

module Core
  class Package < Base
    COLUMN_NAMES = ["id", "partner_id", "package_type_id", "active"]
  end
end
