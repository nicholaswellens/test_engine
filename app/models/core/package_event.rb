# frozen_string_literal: true

module Core
  class PackageEvent < Base
    COLUMN_NAMES = ["id", "package", "application_instance_id", "user_id", "product_id", "extra_data", "kind"]
  end
end
