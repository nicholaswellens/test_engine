# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id                     :bigint           not null, primary key
#  additional_information :text
#  best_before_days       :integer          default(1), not null
#  description            :text
#  ean                    :string           not null, indexed
#  ingredients            :text             not null
#  name                   :string           not null, indexed
#  nutrition_facts        :json
#  price_cents            :integer          not null
#  reheating_information  :json
#  storage_information    :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
class Cup < ApplicationRecord
  def test
    "override works?"
  end
end
