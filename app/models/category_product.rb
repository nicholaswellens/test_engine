# frozen_string_literal: true

# == Schema Information
#
# Table name: categories_products
#
#  id          :bigint           not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint           not null, indexed
#  product_id  :bigint           not null, indexed
#
class CategoryProduct < ApplicationRecord
  self.table_name = "categories_products"

  belongs_to :product
  belongs_to :category

  class << self
    def ransackable_attributes(auth_object = nil)
      ["category_id", "product_id"]
    end

    def ransackable_associations(auth_object = nil)
      []
    end
  end
end
