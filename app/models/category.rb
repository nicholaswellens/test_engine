# frozen_string_literal: true

# == Schema Information
#
# Table name: categories
#
#  id         :bigint           not null, primary key
#  name       :string           not null, indexed
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Category < ApplicationRecord
  has_many :categories_products, dependent: :destroy, class_name: "CategoryProduct", inverse_of: :category
  has_many :products, through: :categories_products

  class << self
    def ransackable_attributes(auth_object = nil)
      ["name"]
    end

    def ransackable_associations(auth_object = nil)
      []
    end
  end
end
