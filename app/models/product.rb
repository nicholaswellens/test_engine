# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id                     :bigint           not null, primary key
#  additional_information :text
#  best_before_days       :integer          default(1), not null
#  description            :text
#  ean                    :string           not null, indexed
#  ingredients            :text             not null
#  name                   :string           not null, indexed
#  nutrition_facts        :json
#  price_cents            :integer          not null
#  reheating_information  :json
#  storage_information    :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
class Product < ApplicationRecord
  has_many :package_types_products, dependent: :destroy, class_name: "PackageTypeProduct", inverse_of: :product
  has_many :categories_products, dependent: :destroy, class_name: "CategoryProduct", inverse_of: :product
  has_many :categories, through: :categories_products
  has_many :allergens_products, dependent: :destroy, class_name: "AllergenProduct", inverse_of: :product
  has_many :allergens, through: :allergens_products
  monetize :price_cents

  has_one_attached :image

  class << self
    def ransackable_attributes(auth_object = nil)
      ["ean", "ingredients", "name"]
    end

    def ransackable_associations(auth_object = nil)
      []
    end
  end

  # For now we will have only one
  def package_types_product
    package_types_products.first
  end
end
