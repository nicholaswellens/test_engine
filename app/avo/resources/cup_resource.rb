# frozen_string_literal: true

class CupResource < Avo::BaseResource
  self.title = :name
  self.includes = []
  self.model_class = "Cup"
  # self.search_query = -> do
  #   scope.ransack(id_eq: params[:q], m: "or").result(distinct: false)
  # end

  field :id, as: :id
  field :name, as: :text, required: true
  field :description, as: :textarea
end
