# frozen_string_literal: true

class PackageTypeProductResource < Avo::BaseResource
  self.title = :id
  self.includes = [:product]
  self.search_query = -> do
    scope.ransack(id_eq: params[:q], m: "or").result(distinct: false)
  end

  field :id, as: :id
  field :package_type_id,
    as: :select,
    name: "Package Type",
    options: ->(model:, resource:, view:, field:) { # rubocop:disable UnusedBlockArgument
      Core::PackageType.all.map do |type|
        [type.size, type.id]
      end
    },
    only_on: :forms
  field :package_type_id, as: :text, hide_on: :forms
  field :quantity, as: :number
  field :people, as: :number
  field :product, as: :belongs_to
end
