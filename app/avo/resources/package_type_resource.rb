# frozen_string_literal: true

class PackageTypeResource < Avo::BaseResource
  self.title = :id
  self.includes = []
  self.model_class = "Core::PackageType"
  # self.search_query = -> do
  #   scope.ransack(id_eq: params[:q], m: "or").result(distinct: false)
  # end

  field :id, as: :id
  # Generated fields from model
  field :name, as: :text
  field :size, as: :text
  field :volume, as: :number
  # add fields here
end
