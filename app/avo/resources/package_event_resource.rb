# frozen_string_literal: true

class PackageEventResource < Avo::BaseResource
  self.title = :id
  self.includes = []
  self.model_class = "Core::PackageEvent"
  # self.search_query = -> do
  #   scope.ransack(id_eq: params[:q], m: "or").result(distinct: false)
  # end

  field :id, as: :id
  # Generated fields from model
  field :package, as: :number do |model|
    model.package.id
  end
  field :application_instance_id, as: :number
  field :user_id, as: :number
  field :product_id, as: :number
  field :extra_data, as: :key_value
  field :kind, as: :text
end
