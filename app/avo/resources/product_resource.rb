# frozen_string_literal: true

class ProductResource < Avo::BaseResource
  self.title = :name
  self.includes = []
  self.search_query = -> do
    scope.ransack(id_eq: params[:q], m: "or").result(distinct: false)
  end

  field :id, as: :id
  field :name, as: :text, required: true
  field :price, as: :number, format_using: -> { view == :new || view == :edit ? value : value&.format }, required: true
  field :ean, as: :text, required: true
  field :ingredients, as: :textarea, required: true
  field :description, as: :textarea
  field :reheating_information, as: :textarea
  field :storage_information, as: :textarea
  field :additional_information, as: :textarea
  field :nutrition_facts,
    as: :key_value,
    key_label: "Nutriente",
    value_label: "Dose",
    default: {
      "Energia (kJ/kcal)": "0 / 0",
      "Hidratos de Carbono (g)": 0,
      "dos quais açúcares (g)": 0,
      "Lípidos (g)": 0,
      "dos quais saturados (g)": 0,
      "Proteínas (g)": 0,
    }
  field :package_types_products, as: :has_many
  field :categories, as: :has_many, through: :categories_products
  field :allergens, as: :has_many, through: :allergens_products
  field :best_before_days, as: :number, required: true
  field :image, as: :file, is_image: true
end
