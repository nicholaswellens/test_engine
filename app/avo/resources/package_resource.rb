# frozen_string_literal: true

class PackageResource < Avo::BaseResource
  self.title = :id
  self.includes = []
  self.model_class = "Core::Package"
  # self.search_query = -> do
  #   scope.ransack(id_eq: params[:q], m: "or").result(distinct: false)
  # end

  field :id, as: :id
  field :partner_id, as: :number
  field :package_type, as: :number do |model|
    model.package_type.id
  end
  field :active, as: :boolean
end
