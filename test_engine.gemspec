require_relative "lib/test_engine/version"

Gem::Specification.new do |spec|
  spec.name        = "test_engine"
  spec.version     = TestEngine::VERSION
  spec.authors     = [""]
  spec.email       = [""]
  spec.homepage    = "https://gitlab.com/nicholaswellens/test_engine"
  spec.summary     = "Summary of TestEngine."
  spec.description = "Description of TestEngine."

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = "https://gitlab.com/nicholaswellens/test_engine"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/nicholaswellens/test_engine"
  spec.metadata["changelog_uri"] = "https://gitlab.com/nicholaswellens/test_engine"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 7.0.7.2"
  spec.add_dependency "avo", ">= 2.40"
  spec.add_dependency "activeresource", ">= 6.0.0"
end
