module TestEngine
  class Engine < ::Rails::Engine
    avo_path = "#{config.root}/app/avo"
    config.autoload_paths += [avo_path]

    config.to_prepare do
      Dir.glob("#{avo_path}/resources/*.rb").sort.each do |resource|
        load(resource)
      end
    end
  end
end