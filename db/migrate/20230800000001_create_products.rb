# frozen_string_literal: true

class CreateProducts < ActiveRecord::Migration[7.0]
  def change
    create_table(:products) do |t|
      t.string(:name, null: false)
      t.integer(:price_cents, null: false)
      t.string(:ean, null: false)
      t.text(:description)
      t.text(:ingredients, null: false)
      t.text(:reheating_information)
      t.text(:storage_information)
      t.text(:additional_information)
      t.json(:nutrition_facts)
      t.integer(:best_before_days, null: false, default: 1)

      t.timestamps
    end

    add_index(:products, :ean, unique: true)
    add_index(:products, :name)
  end
end
