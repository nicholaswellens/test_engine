# frozen_string_literal: true

class CreatePackageTypesProducts < ActiveRecord::Migration[7.0]
  def change
    create_table(:package_types_products) do |t|
      t.references(:product, null: false, foreign_key: true)
      t.bigint(:package_type_id, null: false)
      t.integer(:quantity, null: false)
      t.integer(:people, null: false)

      t.timestamps
    end
  end
end
