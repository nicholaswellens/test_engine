# frozen_string_literal: true

class CreateCups < ActiveRecord::Migration[7.0]
  def change
    create_table :cups do |t|
      t.string(:name)
      t.text(:description)
      t.timestamps
    end
  end
end
